# Domain Design

## - Domain Namespace 설정

![Domain Namespace](images/domain-namespace.png){width="50%"}

> | 이름   | 설명               |
> | ------ | ------------------ |
> | Prefix | Unique한 도메인 명 |
> | Label  | 도메인 설명        |

## - Infrastructure 생성

![Domain Infrastructure](images/domain-infrastructure.png){width="50%"}

> | 이름             | 설명                      |
> | ---------------- | ------------------------- |
> | Local Identifier | DB Table or Colleciton 명 |

---

## - Root Entity & Entity 생성

![Domain](images/domain.png){width="50%"}

![Domain Entity](images/domain-root-entity.png){width="50%"}

> | 이름                          | 속성                | 비고                                  |
> | ----------------------------- | ------------------- | ------------------------------------- |
> | servicingOrderType            | selectionElement    | ["PAYMENT_CASH_WITHDRAWALS"]          |
> | thirdPartyReference           | ThirdPartyReference | Entity                                |
> | customerReference             | CustomerReference   | Entity                                |
> | servicingOrderWorkDescription | Text                |                                       |
> | servicingOrderWorkProduct     | selectionElement    | ["PAYMENT]                            |
> | processStartDate              | date                |                                       |
> | processEndDate                | date                |                                       |
> | servicingOrderWorkResult      | selectionElement    | ["CANCELED", "PROCESSING", "SUCCESS"] |

---

## - Commands 생성

> | 이름             | 설명                             |
> | ---------------- | -------------------------------- |
> | Type             | Command Type (Factory, Instance) |
> | Local Identifier | Command Operation 이름           |
> | Label            | Command Label                    |

### 1. Factory Command

> ## CreateServicingOrderProducer Command
>
> | 이름                          | 속성                | 비고                                  |
> | ----------------------------- | ------------------- | ------------------------------------- |
> | servicingOrderType            | selectionElement    | ["PAYMENT_CASH_WITHDRAWALS"]          |
> | thirdPartyReference           | ThirdPartyReference | Entity                                |
> | customerReference             | CustomerReference   | Entity                                |
> | servicingOrderWorkDescription | Text                |                                       |
> | servicingOrderWorkProduct     | selectionElement    | ["PAYMENT]                            |
> | processStartDate              | date                |                                       |
> | processEndDate                | date                |                                       |
> | servicingOrderWorkResult      | selectionElement    | ["CANCELED", "PROCESSING", "SUCCESS"] |
> | updateID                      | selectionElement    | ["CANCELED", "PROCESSING", "SUCCESS"] |

### 2. Instance Command

> ## UpdateServicingOrderProducer Command
>
> | 이름                          | 속성                | 비고                                  |
> | ----------------------------- | ------------------- | ------------------------------------- |
> | servicingOrderType            | selectionElement    | ["PAYMENT_CASH_WITHDRAWALS"]          |
> | thirdPartyReference           | ThirdPartyReference | Entity                                |
> | customerReference             | CustomerReference   | Entity                                |
> | servicingOrderWorkDescription | Text                |                                       |
> | servicingOrderWorkProduct     | selectionElement    | ["PAYMENT]                            |
> | processStartDate              | date                |                                       |
> | processEndDate                | date                |                                       |
> | servicingOrderWorkResult      | selectionElement    | ["CANCELED", "PROCESSING", "SUCCESS"] |
> | updateID                      | selectionElement    | ["CANCELED", "PROCESSING", "SUCCESS"] |

---

## - Service 생성

![Domain Service](images/domain-service.png){width="50%"}

> | 이름             | 설명                   |
> | ---------------- | ---------------------- |
> | Type             | Service Type           |
> | Local Identifier | Service Operation 이름 |
> | Label            | Service Label          |

> # WithdrawalDomainService Service
>
> ## Input
>
> | 이름          | 속성 | 비고 |
> | ------------- | ---- | ---- |
> | accountNumber | Text |      |
> | amount        | Test |      |

> ## Output
>
> | 이름                     | 속성             | 비고                                  |
> | ------------------------ | ---------------- | ------------------------------------- |
> | trasactionId             | Text             |                                       |
> | servicingOrderWorkResult | selectionElement | ["CANCELED", "PROCESSING", "SUCCESS"] |

---

## - Events 생성

![Domain Event](images/domain-event.png){width="50%"}

> | 이름                | 설명            |
> | ------------------- | --------------- |
> | Local Identifier    | Event Operation |
> | Label               | Event Label     |
> | Event Topic Binding | Kafka Topic     |

![Domain Event](images/domain-event-detail.png){width="50%"}

> ## Event Payload
>
> | 이름      | 속성                    | 비고                         |
> | --------- | ----------------------- | ---------------------------- |
> | eventType | Text                    | ["PAYMENT_CASH_WITHDRAWALS"] |
> | payload   | ServicingOrderProcedure | Entity                       |

---

## - Agents 생성

![Domain Agaent](images/domain-agent.png){width="50%"}

> | 이름             | 설명             |
> | ---------------- | ---------------- |
> | Type             | Agent Type       |
> | Trigger Event    | 수신할 이벤트    |
> | Local Identifier | 사용할 Operation |

![Domain Agent](images/domain-agent-detail.png){width="50%"}
